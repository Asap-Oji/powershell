﻿<#

.SYNOPSIS   
    Script qui simule le fonctionnement d'un automate.
	
.DESCRIPTION
    Ce script a pour but de simuler le fonctionnement d'un automate afin de créer un script qui pourrait être intégré à un distributeur de boisson.

.PARAMETER name
   $filePath = chemin relatif de mon script.

.NOTES
    Name: Automate.ps1 
    Author: Yann
    Requires: Tested with PowerShell v4 only. 
	Version History:
	1.0 - 09.06.2017 - Initial Release.

#>
param([float]$nb,[switch]$stop,[switch]$select)

$csv = "C:\Automate\Automate.csv"
$csvFile = Import-csv $csv -Delimiter ";"

$monnaie = Get-content -path "C:\Automate\monnaie.txt"
$monnaie = [float]$monnaie

foreach($ligne in $csvFile){
    $prix = $ligne.prix
    $emplacement = $ligne.position

    if($select.IsPresent){
        if($monnaie -ge $prix){
            if($nb -eq $emplacement){
                if($ligne.nombre -le 0){
                    Write-Host "emplacement vide!"
                }
                else{
                    $monnaie = $monnaie - $prix
                    $ligne.nombre = $ligne.nombre - 1
                    
                }
            } 
        }
    }
}
if($select.IsPresent){
#ne sais pas comment faire pour faire l'inverse de ispresent
}
else{
    if([string]$nb -eq "0.5" -or [string]$nb -eq "1" -or [string]$nb -eq "1.5" -or [string]$nb -eq "2" -or [string]$nb -eq "2.5"){
        $monnaie = $monnaie+$nb 
    }
    else{
    write-host "`r impossible`r?"
    }
}
if($stop.IsPresent){
    $monnaie = 0
}

Write-Host "Vous avez actuellement " $monnaie ".-"
$csvFile| export-csv -delimiter ";" -path "C:\Automate\Automate.csv" -notype
[string]$monnaie | Out-file "C:\Automate\monnaie.txt"
