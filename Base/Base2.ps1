﻿$choix = Read-Host "Veuillez choisir le dossier"
cd c:\
$destination = "c:\$choix"
new-item $destination\ESSAIS\A -type directory -force
cd $destination\ESSAIS
new-item -type file ESSAI_1.txt -force
Copy-Item $destination\ESSAIS\ESSAI_1.txt -destination C:\ESSAIS\COPIE_ESSAI.txt
Copy-Item $destination\ESSAIS\ESSAI_1.txt -destination C:\ESSAIS\TEST_1.txt
new-item $destination\ESSAIS\B -type directory -force
new-item $destination\ESSAIS\B\B1 -type directory -force
new-item $destination\ESSAIS\B\B2 -type directory -force
new-item $destination\ESSAIS\B\B2\B2A -type directory -force
new-item $destination\ESSAIS\B\B2\B2B -type directory -force
new-item $destination\ESSAIS\B\B2\B2C -type directory -force