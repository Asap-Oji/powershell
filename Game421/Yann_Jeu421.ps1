﻿ # Titre   : Jeu du 421
 # Version : 08/05/2017
 # Màj     : 08/05/2017
 # Auteur  : yann.apotheloz@cpnv.ch
 # Configuration de l'IHM
 
 #Initialisation des variables
 $nbWin = 0;
 $nbLost = 0;

 #Affichage fenetre + redimensionnement
 $mafenetre = New-Object System.Windows.Forms.Form;
 $mafenetre.width = 300;
 $mafenetre.height = 180;
 
 #Affichage text fenetre
 $mafenetre.Text = "Stats du jeu 421";
 
 #Constructions des boutons et labels

 #Affichage et placement du bouton "lancer"
 $btnLancer = New-Object System.Windows.Forms.Button;
 $btnLancer.Text = "Lancer les dés";
 $btnLancer.location = new-object system.drawing.size(40,20);
 $btnLancer.size = New-Object System.Drawing.Size(100,40)

 #Affichage et placement du bouton "quitter"
 $btnQuitter = New-Object System.Windows.Forms.Button;
 $btnQuitter.Text = "Quitter";
 $btnQuitter.location = new-object system.drawing.size(180,20);
 $btnQuitter.size = New-Object System.Drawing.Size(100,40)

 #Labels
 #Labels quand on a gagné
 $lblWin = New-Object system.windows.forms.label;
 $lblWin.location = New-Object system.drawing.size(180, 80);
 $lblWin.size = New-Object system.drawing.size(100,20);
 $lblWin.Text = "Gagnés: " + $nbWin;
  
  #Labels quand on a perdu
 $lblLost = New-Object system.windows.forms.label;
 $lblLost.location = New-Object system.drawing.size(40, 80);
 $lblLost.size = New-Object system.drawing.size(100,20);
 $lblLost.Text = "Perdus: " + $nbLost;

 #Ajouter des boutons et labels à notre fenetre
 $mafenetre.controls.add($btnLancer);
 $mafenetre.controls.add($btnQuitter);
 $mafenetre.controls.add($lblWin);
 $mafenetre.controls.add($lblLost);
 
 $btnQuitter.Add_Click({
 $mafenetre.Close();
 });
 $btnLancer.Add_Click(
 {
     for($i=0; $i -lt 2000; $i++)
     {
        $rand1 = Get-Random -Minimum 1 -Maximum 7;
        $rand2 = Get-Random -Minimum 1 -Maximum 7;
        $rand3 = Get-Random -Minimum 1 -Maximum 7;

        Write-Host ($rand1,$rand2,$rand3) -Separator "-"

        if ($rand1 -eq "4" -and $rand2 -eq "2" -and $rand3 -eq "1")
        {
            $nbWin ++;
            Write-Host "You win"; 
        }
        else
        {
            $nbLost++;
        }

        $lblLost.Text = "Perdu : " + $nbLost;
        $lblWin.Text = "Win : " + $nbWin;

        $mafenetre.Refresh();
     }
 }
 ); 

 #Affichage
 $mafenetre.showdialog();

