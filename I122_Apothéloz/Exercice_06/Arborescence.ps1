﻿<#

.SYNOPSIS   
    a brief explanation of what the script or function does.
	
.DESCRIPTION
    a more detailed explanation of what the script or function does.

.PARAMETER name
    an explanation of a specific parameter. Replace name with the parameter name. You can have one of these sections for each parameter the script or function uses.

.EXAMPLE
    an example of how to use the script or function. 
    You can have multiple .EXAMPLE sections if you want to provide more than one example.

.LINK    
    a cross-reference to another help topic; you can have more than one of these. 
    If you include a URL beginning with http:// or https://, the shell will open that URL when the Help command’s –online parameter is used.

.NOTES
    any miscellaneous notes on using the script or function. 
    Name: NameOfTheFile.ps1 
    Author: The author of the script :-)
    Requires: Tested with PowerShell v4 only. 
	Version History:
	1.0 - 13/11/2016 - Initial Release.

#>
CLEAR

$destination = "C:\";
$filename = "P:\2ème année\4ème trimestre\Branche Technique\I-CT_122_AUTOMATISER_PROCEDURE\GitPush\I122_Apothéloz\Exercice_06\Liste_Eleves.csv";
$eleves = Import-csv -path $filename -delimiter ";";

if(test-path $destination\TMP122)
{
    Remove-Item -Path $destination\TMP122 -Recurse
}

Foreach($eleve in $eleves)
{
    $classe = $eleve.Classe;
    $nom = $eleve.Nom;
    $prenom = $eleve.Prenom;
    
    if ($classe -eq "")
    {
        Write-Host "Erreur : l'élève"$prenom $nom "ne possède pas de classe" -ForegroundColor Red;
    }
    else
    {
        if(test-path $destination\TMP122\$classe) 
        {
            New-Item -Path "$destination\TMP122\$classe\$nom $prenom" -ItemType directory
        }
        else
        {
            New-Item -Path $destination\TMP122\$classe -ItemType directory
            New-Item -Path "$destination\TMP122\$classe\$nom $prenom" -ItemType directory
        }
        copy-item -Path $pSScriptRoot\SKEL\* -Destination "$destination\TMP122\$classe\$nom $prenom\" -recurse
    }     
}

